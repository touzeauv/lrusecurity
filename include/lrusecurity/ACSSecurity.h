#ifndef LRUSECURITY_ACSSECURITY_H_H_
#define LRUSECURITY_ACSSECURITY_H_H_
#include <otawa/icache/features.h>
#include <otawa/icat3/features.h>
#include <otawa/base.h>
#include <set>

namespace lrusecurity
{
    /*
  struct Compa_access
  {
      bool operator()(const otawa::icache::Access a, const otawa::icache::Access b) const {return &a < &b;}
  };
    */

  class ACSSecurity : public elm::AllocArray<std::set <const otawa::icache::Access *>>
{
 public:
  ACSSecurity() = default;

 ACSSecurity(int n) : elm::AllocArray<std::set <const otawa::icache::Access *>>(n){
    for (int i = 0; i < n; i++)
      (*this)[i] = std::set <const otawa::icache::Access *>();
  }
 ACSSecurity(int n, const otawa::icache::Access* access) : elm::AllocArray<std::set <const otawa::icache::Access *>>(n) {
    for (int i = 0; i < n; i++){
      (*this)[i] = std::set <const otawa::icache::Access *>();
      (*this)[i].insert(access);
    }
  }
  void print(int set, const otawa::icat3::LBlockCollection& coll, elm::io::Output& out= elm::cout) const;
};
}// namespace lrusecurity

#endif //LRUSECURITY_ACSSECURITY_H_H_
