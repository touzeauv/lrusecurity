#ifndef LRUSECURITY_FEATURES_H_
#define LRUSECURITY_FEATURES_H_

#include <string>

#include <elm/data/Array.h>

#include <otawa/proc/AbstractFeature.h>
#include <otawa/icat3/features.h>
#include <lrusecurity/ACSSecurity.h>
namespace lrusecurity {

// Exist-Hit with previous access
extern otawa::p::feature EXIST_HIT_PREV_ANALYSIS_FEATURE;
 extern otawa::p::id<otawa::icat3::Container<ACSSecurity> > EXIST_HIT_PREV_INIT;
 extern otawa::p::id<otawa::icat3::Container<ACSSecurity> > EXIST_HIT_PREV_IN;

enum class SecurityCategory{
   SAFE,
   UNSAFE
};
 class CacheState;
 class ACSSManager
{
  public:
	ACSSManager(otawa::WorkSpace *ws);
	~ACSSManager(void);
	void start(otawa::Block *b);
	void update(const otawa::icache::Access& acc);
	int existHitAge(const otawa::icat3::LBlock *lb);
	void print(const otawa::icat3::LBlock *lb, elm::io::Output& out = elm::cout);

private:
	CacheState& use(int set);
	const otawa::icat3::LBlockCollection& coll;
	elm::Vector<int> used;
	otawa::Block *_b;
	CacheState **_states;
};

//Specifying which calls are making the program unsafe 
extern otawa::p::feature SECURITY_CATEGORY_FEATURE;
extern otawa::p::id<SecurityCategory> SECURITY_CATEGORY;
 
} // namespace lrusecurity

#endif // LRUMC_FEATURES_H
