#include <otawa/proc/ProcessorPlugin.h>

namespace lrusecurity {

using namespace elm;
using namespace otawa;

class Plugin: public ProcessorPlugin {
public:
	Plugin(void): ProcessorPlugin("lrusecurity", Version(1, 0, 0), OTAWA_PROC_VERSION) { }
};

} // namespace lrusecurity

lrusecurity::Plugin lrusecurity_plugin;
ELM_PLUGIN(lrusecurity_plugin, OTAWA_PROC_HOOK);

