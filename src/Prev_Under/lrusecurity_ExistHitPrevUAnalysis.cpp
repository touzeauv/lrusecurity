#include "ExistHitPrevUAnalysis.h"

#include <chrono>

#include <otawa/ai/ArrayStore.h>
#include <otawa/ai/SimpleAI.h>
#include <otawa/icache/features.h>
#include <otawa/icat3/features.h>

#include <lrusecurity/features.h>

#include "ExistHitPrevUAdapter.h"
#include "ExistHitPrevUDomain.h"

using namespace otawa;

namespace lrusecurity
{

ExistHitPrevUAnalysis::ExistHitPrevUAnalysis(p::declare& r) :
		Processor(r),
		_initExistHitPrevU(nullptr),
		_coll(nullptr),
		_cfgs(nullptr)
{
}

void ExistHitPrevUAnalysis::configure(const PropList& props)
{
	Processor::configure(props);
	if(props.hasProp(EXIST_HIT_PREV_INIT))
		_initExistHitPrevU = &EXIST_HIT_PREV_INIT(props);
}

void ExistHitPrevUAnalysis::setup(WorkSpace* ws)
{
	_coll = icat3::LBLOCKS(ws);
	ASSERT(_coll != nullptr);
	_cfgs = INVOLVED_CFGS(ws);
	ASSERT(_cfgs != nullptr);
}

  void ExistHitPrevUAnalysis::processWorkSpace(WorkSpace* ws) //changer les ACS en ACSSecurity
{
	auto start = std::chrono::system_clock::now();

	// prepare containers
	for(CFGCollection::BlockIter b(_cfgs); b(); b++)
		(*EXIST_HIT_PREV_IN(*b)).configure(*_coll);

	// compute ACS
	for(int i = 0; i < _coll->cache()->setCount(); i++) {
		if((*_coll)[i].count()) {
			if(logFor(LOG_FUN))
				log << "\tanalyzing set " << i << io::endl;
			processSet(i, ws);
		}
	}

	auto end = std::chrono::system_clock::now();
	auto elapsed = std::chrono::duration_cast<std::chrono::seconds>(end - start);
	if(logFor(LOG_FUN))
		log << "\tExist Hit Prev Analysis running time: " << elapsed.count() << " s" << io::endl;
}

void ExistHitPrevUAnalysis::destroy(WorkSpace*)
{
	for(CFGCollection::BlockIter b(_cfgs); b(); b++)
		EXIST_HIT_PREV_IN(*b).remove();
}

void ExistHitPrevUAnalysis::processSet(int set, WorkSpace* ws)
{
	// perform the analysis
	ExistHitPrevUAdapter ada(set, _initExistHitPrevU ? &_initExistHitPrevU->get(set) : nullptr, *_coll, *_cfgs, ws);
	ai::SimpleAI<ExistHitPrevUAdapter> ana(ada);
	ana.run();
	// store the results
	for(CFGCollection::BlockIter b(_cfgs); b(); b++)
		if(b->isBasic()) {
		  ada.domain().copy((*EXIST_HIT_PREV_IN(*b))[set], ada.store().get(*b));
			if(logFor(LOG_BLOCK))
				log << "\t\t\t" << *b << ": " << ada.domain().print(ada.store().get(*b)) << io::endl;
		}
}

p::declare ExistHitPrevUAnalysis::reg = p::init("lrusecurity::ExistHitPrevUAnalysis", Version(1, 0, 0))
	.require(icat3::LBLOCKS_FEATURE)
	.require(COLLECTED_CFG_FEATURE)
	.require(icat3::MUST_PERS_ANALYSIS_FEATURE)
	.require(icat3::CATEGORY_FEATURE)
	.require(lrupreanalysis::eh_em::EXIST_HIT_ANALYSIS_FEATURE)
	.provide(EXIST_HIT_PREV_U_ANALYSIS_FEATURE)
	.make<ExistHitPrevUAnalysis>();

p::feature EXIST_HIT_PREV_U_ANALYSIS_FEATURE("lrusecurity::EXIST_HIT_PREV_ANALYSIS_U_FEATURE", p::make<ExistHitPrevUAnalysis>());
}; // namespace lrusecurity
