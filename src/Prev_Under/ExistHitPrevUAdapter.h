
#ifndef LRUSECURITY_EXIST_HIT_PREV_ADAPTER_U_H_
#define LRUSECURITY_EXIST_HIT_PREV_ADAPTER_U_H_

#include <otawa/ai/ArrayStore.h>
#include <otawa/icat3/features.h>
#include <otawa/cfg/CompositeCFG.h>
#include <otawa/cfg/features.h>

#include <lrupreanalysis/features.h>

#include "ExistHitPrevUDomain.h"

namespace lrusecurity
{

class ExistHitPrevUAdapter
{
public:
  using domain_t = ExistHitPrevUDomain;
	using t = typename domain_t::t;
	using graph_t = otawa::CompositeCFG;
	using store_t = otawa::ai::ArrayStore<domain_t, graph_t>;

	ExistHitPrevUAdapter(int set, const t* init, const otawa::icat3::LBlockCollection& coll, const otawa::CFGCollection& cfgs, otawa::WorkSpace* ws);

	inline domain_t& domain(void) { return _domain; }
	inline graph_t& graph(void) { return _graph; }
	inline store_t& store(void) { return _store; }

	void update(const otawa::Bag<otawa::icache::Access>& accs, t& d);
	void update(otawa::Block *v, t& d);

private:
	int _set;

	lrupreanalysis::eh_em::ACSManager _ehManager;
	domain_t _domain;
	graph_t _graph;
	store_t _store;
};

} // namespace lrusecurity

#endif // LRUSECURITY_EXIST_HIT_ADAPTER_U_H_
