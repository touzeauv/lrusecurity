#include "ExistHitPrevUDomain.h"
#include <set>

using namespace otawa;

namespace lrusecurity
{

ExistHitPrevUDomain::ExistHitPrevUDomain(
        const icat3::LBlockCollection& coll,
        int set,
        const t *init):
    _n(coll[set].count()),
    _bot(_n),
	_top(_n, nullptr),//TO CHANGE
    _set(set),
	_coll(coll),
    _A(coll.A()),
    _init(init ? *init : _top),
    _tmp(_n)
{
}

bool ExistHitPrevUDomain::equals(const t& acss, const t& bcss) const
{
  for (int i = 0; i < _n; i++)
    if (acss[i] != bcss[i])
      return false;
  return true;
}

void ExistHitPrevUDomain::join(t& dcss, const t& scss, icat3::ACS d_eh, icat3::ACS s_eh)
{
  for (int i = 0; i < _n; i++) {
    if (s_eh[i] < d_eh[i] || (d_eh[i] == -1 && s_eh[i] != -1)) {
      dcss[i] = scss[i];
    } else if (s_eh[i] == d_eh[i]) {
      dcss[i].insert(scss[i].begin(), scss[i].end());
    }
  }
}

void ExistHitPrevUDomain::fetch(t& acss, const icache::Access& access, lrupreanalysis::eh_em:: ACSManager & ehManager)
{
  icat3::LBlock *lb = icat3::LBLOCK(access);
  int b = lb->index();
  for (int i = 0; i < _n; i++){
    if (i == b){
      acss[i] = std::set<const otawa::icache::Access *>();
      acss[i].insert(&access);
    } else if (ehManager.existHitAge(_coll[_set][i]) == _A)
      acss[i] = std::set<const otawa::icache::Access *>();
  }
}

void ExistHitPrevUDomain::update(const icache::Access& access, t& a, lrupreanalysis::eh_em::ACSManager& ehManager)
{
  switch(access.kind()) {

  case icache::FETCH:
    if(_coll.cache()->set(access.address()) == _set)
      fetch(a, access, ehManager);
      break;

  case icache::PREFETCH:
    if(_coll.cache()->set(access.address()) == _set) {
      copy(_tmp, a);
      fetch(a, access, ehManager);
      join(a, _tmp, icat3::ACS(), icat3::ACS());
    }
    break;

  case icache::NONE:
    break;

  default:
    ASSERT(false);
  }
}

} // namespace lrusecurity
