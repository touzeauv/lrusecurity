
#ifndef LRUSECURITY_EXIST_HIT_DOMAIN_U_H_
#define LRUSECURITY_EXIST_HIT_DOMAIN_U_H_

#include <otawa/icat3/features.h>
#include <otawa/icache/features.h>
#include <elm/io/Output.h>
#include <lrumc/features.h>
#include <lrumc/ACSManager.h>
#include <lrupreanalysis/features.h>
#include <lrusecurity/ACSSecurity.h>
namespace lrusecurity
{

class ExistHitPrevUDomain {
public:
  using t = ACSSecurity;

	ExistHitPrevUDomain(const otawa::icat3::LBlockCollection& coll, int set, const t* init);
	inline const t& bot(void) const { return _bot; }
	inline const t& top(void) const { return _top; }
	inline const t& init(void) const { return _init; }
	inline void print(const t& a, otawa::io::Output& out) const { a.print(_set, _coll, out); }

	inline elm::io::Printable<t, ExistHitPrevUDomain> print(const t& a) const { return elm::io::p(a, *this); }
	inline bool contains(const t& a, int i) { return(!a[i].empty()); }
	inline void copy(t& d, const t& s) { d.copy(s); }
	bool equals(const t& a, const t& b) const;
	void join(t& d, const t& s, otawa::icat3::ACS d_eh, otawa::icat3::ACS s_eh);
	void fetch(t& a, const otawa::icache::Access& ac, lrupreanalysis::eh_em::ACSManager& mayManager);
	void update(const otawa::icache::Access& access, t& a, lrupreanalysis::eh_em::ACSManager& mayManager);

private:
	int _n;
	t _bot, _top;
	otawa::hard::Cache::set_t _set;
	const otawa::icat3::LBlockCollection& _coll;
	int _A;
	const t& _init;
	t _tmp;
};

} // namespace lrusecurity


#endif // LRUSECURITY_EXIST_HIT_DOMAIN_U_H_
