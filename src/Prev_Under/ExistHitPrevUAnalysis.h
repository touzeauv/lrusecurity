
#ifndef LRUSECURITY_EXIST_HIT_ANALYSIS_U_H_
#define LRUSECURITY_EXIST_HIT_ANALYSIS_U_H_

#include <otawa/proc/Processor.h>
#include <otawa/icat3/features.h>
#include <otawa/cfg/features.h>
#include <lrusecurity/ACSSecurity.h>
namespace lrusecurity
{

class ExistHitPrevUAnalysis: public otawa::Processor
{
public:
	static otawa::p::declare reg;
	ExistHitPrevUAnalysis(otawa::p::declare& r = reg);
protected:

	void configure(const otawa::PropList& props) override;
	void setup(otawa::WorkSpace* ws) override;
	void processWorkSpace(otawa::WorkSpace* ws) override;
	void destroy(otawa::WorkSpace* ws) override;

private:
	void processSet(int i, otawa::WorkSpace* ws);

	const otawa::icat3::Container<ACSSecurity>* _initExistHitPrevU;
	const otawa::icat3::LBlockCollection* _coll;
	const otawa::CFGCollection* _cfgs;
};

/**
 * Perform the ACS analysis for the Exist-Hit domain. For cache block, it associates an upper bound on block age that holds on at least on path
 * @par Properties
 * @li @ref EXIST_HIT_PREV_IN
 *
 * @par Configuraiton
 * @li @ref EXIST_HIT_PREV_INIT
 *
 * @par Implementation
 * @li @ref ExistHitPrevUAnalysis
 *
 * @ingroup lrusecurity
 */
extern otawa::p::feature EXIST_HIT_PREV_U_ANALYSIS_FEATURE;

/**
 * ACSSecurity for the Exist-Hit analysis at the entry of the corresponding block or edge.
 *
 * @par Feature
 * @li @ref EXIST_HIT_ANALYSIS_PREV_FEATURE
 *
 * @par Hooks
 * @li @ref Block
 * @li @ref Edge
 *
 * @ingroup lrusecurity
 */
 extern otawa::p::id<otawa::icat3::Container<ACSSecurity> > EXIST_HIT_PREV_IN;


/**
 * Initial state for Exist-Miss instruction cache analysis.
 *
 * @par Hook
 * @li Feature configuration.
 *
 * @par Feature
 * @li @ref EXIST_HIT_ANALYSIS_PREV_FEATURE
 *
 * @ingroup lrusecurity
 */
 extern otawa::p::id<otawa::icat3::Container<ACSSecurity> > EXIST_HIT_PREV_INIT;

}; // namespace lrusecurity

#endif // LRUSECURITY_EXIST_HIT_ANALYSIS_H_
