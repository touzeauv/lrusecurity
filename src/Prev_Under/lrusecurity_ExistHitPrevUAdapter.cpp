#include "ExistHitPrevUAdapter.h"

#include <lrupreanalysis/ExistHitDomain.h>

using namespace otawa;

namespace lrusecurity
{

ExistHitPrevUAdapter::ExistHitPrevUAdapter(
		int set,
		const t* init,
		const icat3::LBlockCollection& coll,
		const CFGCollection& cfgs,
		otawa::WorkSpace* ws) :
    _set(set),
	_ehManager(ws),
	_domain(coll, set, init),
	_graph(cfgs),
	_store(_domain, _graph)
{
}

void ExistHitPrevUAdapter::update(const Bag<icache::Access>& accs, t& d)
{
	for(int i = 0; i < accs.count(); i++) {
		_domain.update(accs[i], d, _ehManager);
		_ehManager.update(accs[i]);
	}
}

void ExistHitPrevUAdapter::update(Block *v, t& d)
{
	_domain.copy(d, _domain.bot());
	lrupreanalysis::eh_em::ExistHitDomain::t d_eh;

	t s;

	bool first = true;
	// update and join along edges
	for(auto e = _graph.preds(v); e(); e++) {
		Block *w = e->source();
		_domain.copy(s, _store.get(w));

		_ehManager.start(w);

		if(first) {
		  first = false;
		  _ehManager.ehDomain(_set).copy(d_eh, _ehManager.ehDomain(_set).bot());
		}

		// apply block
		{
			const Bag<icache::Access>& accs = icache::ACCESSES(w);
			if(accs.count() > 0)
				update(accs, s);
		}

		// apply edge
		{
			const Bag<icache::Access>& accs = icache::ACCESSES(*e);
			if(accs.count() > 0)
				update(accs, s);
		}


		// merge result
		lrupreanalysis::eh_em::ExistHitDomain::t* tmp = _ehManager.ehValue(_set);
		ASSERT(tmp && "tmp null");
		lrupreanalysis::eh_em::ExistHitDomain::t& s_eh = *tmp;
        ASSERT(d_eh.count() == s_eh.count());
		_domain.join(d, s, d_eh, s_eh);
		_ehManager.ehDomain(_set).join(d_eh, s_eh);
	}
}

} // namespace lrusecurity
