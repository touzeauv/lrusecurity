#include <lrupreanalysis/features.h>
#include <lrusecurity/features.h>
#include <elm/sys/Path.h>
#include <elm/sys/System.h>

#include <otawa/proc/BBProcessor.h>
#include <otawa/proc/CFGProcessor.h>
#include <otawa/prog/Process.h>
#include <lruexact/features.h>
#include <set>
#include <iterator>

#include "Graphic/AccMatching.h"
#include "Graphic/DotModificator.h"

//#include "SecCFGOutput.h"
using namespace otawa;

namespace lrusecurity
{

class SecurityDisplayer : public BBProcessor
{
public:
	static otawa::p::declare reg;

	SecurityDisplayer() :
		otawa::BBProcessor(reg),
		_path(""),
		_toFile(false),
		_stream(nullptr),
		_line(false),
        // Modification pour affichage en dot
        _modificator(new DotModificator("result.dot", new AccMatcher))   //TODO: changer l'accès au fichier
	{
	}

	virtual void configure(const PropList& props) override
	{
		otawa::BBProcessor::configure(props);
		if(props.hasProp(lruexact::CLASSIFICATION_PATH))
		  _path = lruexact::CLASSIFICATION_PATH(props);
		if(props.hasProp(lruexact::CLASSIFICATION_TO_FILE))
		  _toFile = lruexact::CLASSIFICATION_TO_FILE(props);
	}

    inline DotModificator* getModificator(void){return _modificator;};

protected:

	virtual void setup(WorkSpace *ws) override
	{
		// Check if source is available
		if(ws->isProvided(SOURCE_LINE_FEATURE))
			_line = true;
		if(logFor(LOG_PROC))
			log << "\tsource/line information " << (_line ? "" : "not ") << "available\n";

		if(!_path && _toFile)
			_path = _ << "access_classification.txt";

		if(_path) {
			try {
				_stream = elm::sys::System::createFile(_path);
			}
			catch(elm::sys::SystemException& e) {
				throw ProcessorException(*this, _ << "cannot open \"" << _path << "\"");
			}

			_out.setStream(*_stream);
		}

		_out << "ACCESS\t\tKIND\tCATEGORY\t\tBB\t";
		if(_line)
			_out << "\tLINE";

		_out << io::endl;

	}

    virtual void processAll(WorkSpace *ws) override
    {
        const CFGCollection *coll = INVOLVED_CFGS(ws);
        AccMatcher *match = getModificator() -> getMatcher();
        match->setupColl(coll);

        CFGProcessor::processAll(ws);
    }

	virtual void processCFG(WorkSpace* ws, CFG* cfg) override
	{
		_out << "FUNCTION " << cfg->label() << io::endl;
		BBProcessor::processCFG(ws, cfg);
	}

	virtual void processBB(WorkSpace* ws, CFG* cfg, Block* b) override
	{
		if(!b->isBasic())
			return;

		BasicBlock* bb = b->toBasic();

		for(Block::EdgeIter edgeIter(bb->ins()); edgeIter(); ++edgeIter) {
			const Edge* e = *edgeIter;
			const Bag<icache::Access>& bag = otawa::icache::ACCESSES(e);
			processBag(ws, cfg, bag, bb);
		}

		const Bag<icache::Access>& bag = otawa::icache::ACCESSES(bb);
		processBag(ws, cfg, bag, bb);
	}

	void processBag(WorkSpace* ws, CFG* cfg, const Bag<icache::Access>& bag, BasicBlock* bb)
	{
		for(int i = 0; i < bag.size(); ++i)
			processAccess(ws, cfg, &bag[i], bb);
	}

  void processAccess(WorkSpace* ws, CFG* cfg, const otawa::icache::Access* access, BasicBlock* bb)
	{
	       SecurityCategory cat = SECURITY_CATEGORY(access);
	       if(cat == SecurityCategory::UNSAFE){
		_out << access->address() << "\t";
		switch(access->kind()){
			case otawa::icache::NONE: _out << "NONE\t"; break;
			case otawa::icache::FETCH: _out << "FETCH\t"; break;
			case otawa::icache::PREFETCH: _out << "PREFETCH\t"; break;
		}
			_out << "UNSAFE ";
			int set = otawa::icat3::LBLOCK(access)->set();

            int number = 0;
			if (bb->hasProp(EXIST_HIT_PREV_IN)) {
			  ACSSecurity prev = (*EXIST_HIT_PREV_IN(bb))[set];
			  int index = otawa::icat3::LBLOCK(access)->index();
              number = prev[index].size();
              std::set<const otawa::icache::Access *>::iterator iter;

			  for (iter = prev[index].begin(); iter != prev[index].end(); iter++){
                  if (*iter){
			        _out << (**iter).address() << "\n\t\t\t\t\b";
                  } else {
			        _out << "00000000" << "\n\t\t\t\t\b";
                  }

                  DotModificator *mod = getModificator();
                  mod->modify(cfg, *iter, access->address());
              }

              // REMONTEE
              _out << "\033[" << number << "A";

			}

		_out << "\t\t\tBB " << bb->index() << "\t\t";
		printLine(ws, bb->address(), bb->topAddress().offset());

      // DESCENTE
      for (int i = 0; i < number-1; i++)
        _out << "\n//";
      _out << io::endl;
    }
	}

	void printLine(WorkSpace* ws, Address begin, Address::offset_t offset)
	{

		// Display line
		if(_line) {
			bool one = false;
			Pair<cstring, int> old = pair(cstring(""), 0);
			for(Address addr = begin; addr.offset() < offset; addr = addr + 1) {
				Option<Pair<cstring, int> > res = ws->process()->getSourceLine(addr);
				if(res) {
					if((*res).fst != old.fst) {
						old = *res;
			 			if(one)
			 				_out << ", ";
			 			else
			 				one = true;
			 			_out << old.fst << ':' << old.snd;
					}
					else if((*res).snd != old.snd) {
						old = *res;
						_out << ',' << old.snd;
					}
				}
			}
		}
	}

	virtual void cleanup(WorkSpace*) override
	{
		if(_stream)
			delete _stream;
	}

	elm::io::Output _out;
	elm::sys::Path _path;
	bool _toFile;
	elm::io::OutStream* _stream;
	bool _line;
	bool _mcAnalysis;

    DotModificator *_modificator;
};

p::declare SecurityDisplayer::reg = p::init("lrusecurity::SecurityDisplayer", Version(1, 0, 0))
	.require(SECURITY_CATEGORY_FEATURE)
	.make<SecurityDisplayer>();

} // namespace lrusecurity

