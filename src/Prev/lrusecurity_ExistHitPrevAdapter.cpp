#include "ExistHitPrevAdapter.h"

using namespace otawa;

namespace lrusecurity
{

ExistHitPrevAdapter::ExistHitPrevAdapter(
		int set,
		const t* init,
		const icat3::LBlockCollection& coll,
		const CFGCollection& cfgs,
		otawa::WorkSpace* ws) :
	_ehManager(ws),
	_domain(coll, set, init),
	_graph(cfgs),
	_store(_domain, _graph)
{
}

void ExistHitPrevAdapter::update(const Bag<icache::Access>& accs, t& d)
{
	for(int i = 0; i < accs.count(); i++) {
		_domain.update(accs[i], d, _ehManager);
		_ehManager.update(accs[i]);
	}
}

void ExistHitPrevAdapter::update(Block *v, t& d)
{
	_domain.copy(d, _domain.bot());
	t s;

	// update and join along edges
	for(auto e = _graph.preds(v); e(); e++) {
		Block *w = e->source();
		_domain.copy(s, _store.get(w));

		_ehManager.start(w);

		// apply block
		{
			const Bag<icache::Access>& accs = icache::ACCESSES(w);
			if(accs.count() > 0)
				update(accs, s);
		}

		// apply edge
		{
			const Bag<icache::Access>& accs = icache::ACCESSES(*e);
			if(accs.count() > 0)
				update(accs, s);
		}


		// merge result
		_domain.join(d, s);
	}
}

} // namespace lrusecurity
