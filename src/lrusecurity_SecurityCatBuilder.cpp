#ifndef LRUSECURITY_SECURITY_CAT_BUILDER_H_
#define LRUSECURITY_SECURITY_CAT_BUILDER_H_

#include <otawa/proc/BBProcessor.h>
#include <otawa/icat3/features.h>
#include <otawa/hard/Memory.h>
#include <lrumc/ACSManager.h>
#include <lrusecurity/features.h>
#include <lruexact/features.h>
using namespace otawa;

namespace lrusecurity
{

  class SecurityCatBuilder: public BBProcessor
{
public:
	static p::declare reg;
	SecurityCatBuilder(void): BBProcessor(reg), _ways(0), _man(nullptr)
	{
	}

protected:

	virtual void setup(WorkSpace *ws)
	{
		const otawa::icat3::LBlockCollection* coll = icat3::LBLOCKS(ws);
		ASSERT(coll);
		_ways = coll->A();
		_man = new lrupreanalysis::eh_em::ACSManager(ws);
	}

	virtual void cleanup(WorkSpace*) {
		delete _man;
	}

	virtual void processBB(WorkSpace*, CFG* cfg, Block *v)
	{
		for(Block::EdgeIter e = v->outs(); e(); e++) {
			if(logFor(LOG_BLOCK))
				log << "\t\t\t\tprocess " << *e << io::endl;
			if(v->isSynth() && v->toSynth()->callee())
				_man->start(v->toSynth()->callee()->exit());
			else
				_man->start(v);
			//otawa::sgraph::LoopIdentifier loopv = otawa::sgraph::LoopIdentifier::loopOf(v);
			///otawa::sgraph::LoopIdentifier loope = otawa::sgraph::LoopIdentifier::loopOf(e.sink());
			processAccesses(*icache::ACCESSES(v));
			processAccesses(*icache::ACCESSES(*e));
		}
	}

  void processAccesses(Bag<icache::Access>& accs) {
		for(int i = 0; i < accs.count(); i++) {
		  lrusecurity::SecurityCategory cat;
		        if (lruexact::EXACT_CATEGORY(accs[i]) ==
			    lruexact::ExactCategory::DU)
			    cat = SecurityCategory::UNSAFE;
			else
			  cat = SecurityCategory::SAFE;

			if(logFor(LOG_BLOCK)) {
				log << "\t\t\t\t\tAccess " << accs[i] << " is ";
				switch(cat) {
					case SecurityCategory::SAFE:
						log << "SAFE";
					break;
					case SecurityCategory::UNSAFE:
						log << "UNSAFE";
					break;
				}
				log << io::endl;
			}

			SECURITY_CATEGORY(accs[i]) = cat;
			_man->update(accs[i]);
		}
	}

	int _ways;
        lrupreanalysis::eh_em::ACSManager* _man;
};

p::declare SecurityCatBuilder::reg = p::init("lrusecurity::SecurityCatBuilder", Version(1, 0, 0))
	.extend<BBProcessor>()
	.make<SecurityCatBuilder>()
	.require(otawa::icat3::LBLOCKS_FEATURE)
	.require(otawa::hard::MEMORY_FEATURE)
        .require(lruexact::LRU_CLASSIFICATION_FEATURE)
	.provide(lrusecurity::SECURITY_CATEGORY_FEATURE);


/**
 * This feature ensures that definitely unknown category information is stored on each
 * @ref icache::Access found in the blocks and in the CFG of the current workspace.
 * Such information is made of @ref DUCategory specifying the behaviour of the cache
 * (EH -- Exist Hit, EM -- Exist Miss, DU -- Definitely Unknown (Exist Hit and Exist
 * Miss), NC -- Not-classified).
 *
 * @par Properties
 * @li @ref DU_CATEGORY
 *
 * @par Default implementation
 * @li @ref DUCatBuilder
 *
 * @ingroup lrusecurity
 */
p::feature SECURITY_CATEGORY_FEATURE("lrusecurity::SECURITY_CATEGORY_FEATURE", SecurityCatBuilder::reg);


/**
 * Defines the instruction cache Definitely Unknown category for the @ref icache::Access where
 * this property is set.
 *
 * @par Hooks
 * @li @ref icache::Access
 *
 * @par Features
 * @li @ref DU_CATEGORY_FEATURE
 *
 * @ingroup lrusecurity
 */
p::id<SecurityCategory> SECURITY_CATEGORY("lrusecurity::SECURITY_CATEGORY", SecurityCategory::SAFE);

} // namespace lrusecurity

#endif // LRUSECURITY_SECURITY_CAT_BUILDER_H_

