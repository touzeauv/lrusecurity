#ifndef LRUSECURITY_ACSSECURITY_H_H_
#define LRUSECURITY_ACSSECURITY_H_H_

#include <otawa/icache/features.h>
#include <otawa/icat3/features.h>
#include <otawa/base.h>
namespace lrusecurity
{
  class ACSSecurity : public AllocArray<otawa::Address>
{
 public:
  ACSSecurity();
  ACSSecurity(int n){
    for (int i = 0; i < n; i++)
    (*this)[i] = otawa::Address();
  };
  void print(int set, const otawa::icat3::LBlockCollection& coll, io::Output& out=cout) const;
};
}

#endif //LRUSECURITY_ACSSECURITY_H_H_
