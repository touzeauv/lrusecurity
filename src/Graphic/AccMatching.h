#ifndef LRUSECURITY_ACC_MATCHING_H
#define LRUSECURITY_ACC_MATCHING_H

#include <otawa/cfg/features.h>
#include <otawa/cfg/CFG.h>
#include <otawa/prog/Inst.h>
#include <otawa/icache/features.h>
#include <map>

namespace lrusecurity
{

class AccMatcher
{
/* Cette classe permet de créer un dictionnaire entre Access et BasicBlock auquel appartient cet accès*/
public:
    AccMatcher(void){};
    void setupColl(const otawa::CFGCollection *coll);
    void setupCFG(otawa::CFG *cfg);
    inline void insert(const otawa::icache::Access *acc, otawa::BasicBlock *bb){_match.insert({acc, bb});};

    otawa::BasicBlock *match_access(const otawa::icache::Access *access);

    inline void clean(void){_match = std::map<const otawa::icache::Access *, otawa::BasicBlock *>();};

private:
    std::map<const otawa::icache::Access *, otawa::BasicBlock *> _match;
};

} // namespace lrusecurity

#endif /* ifndef LRUSECURITY_ACC_MATCHING_H */
