#include "AccMatching.h"

using namespace otawa;

namespace lrusecurity
{

void AccMatcher::setupCFG(CFG *cfg)
{
    /* Parcours du CFG pour ajouter les BB au dictionnaire */
    CFG::BlockIter iter_block;

    for (iter_block = cfg->blocks(); iter_block(); iter_block++){
        Block *b = *iter_block;
        if (b->isBasic()){
            BasicBlock *bb = b->toBasic();
            const Bag<icache::Access>& bag = otawa::icache::ACCESSES(bb);
            for (int i = 0; i < bag.size(); i++){
                insert(&bag[i], bb);
            }
        }
    }
}

void AccMatcher::setupColl(const CFGCollection *collection)
{
    for(auto cfg: *collection) {
        AccMatcher::setupCFG(cfg);
    }
}

otawa::BasicBlock* AccMatcher::match_access(const otawa::icache::Access *access)
{
    /* On cherche le BB associé à l'accès*/
    auto search = this->_match.find(access);

    if (search != _match.end())
        return search->second;
    else
        cerr << "Unable to find access\n";
        cerr << access->address();
        cerr << "\n";
        return NULL;
}

} // namespace lrusecurity
