#include <iostream>
#include <fstream>
#include <string>

#include "DotModificator.h"

namespace lrusecurity
{

void displayString(string str, std::ofstream *flux)
{
    /* Cette fonction permet de placer une string elm dans std::ofstream*/
    for (String::Iter i = str.begin(); i!= str.end(); i++)
        *flux << *i;
}


void DotModificator::modify(otawa::CFG *cfg, const otawa::icache::Access *access, otawa::Address address)
{
    std::ofstream flux(_filename, std::ios::app);

    if (flux){
        // Permet de mettre des nodes au même niveau
        if (isFirst()) {
            flux << "rankdir = TB;" << io::endl;
        }

        //Copie de l'addresse pour affichage
        string addr = _  << address;

        if (access){
            // On récupère le bloc correspondant à l'accès grâce à l'InstMatcher
            otawa::BasicBlock *bb_match = getMatcher()->match_access(access);
            if (bb_match != NULL) {
                // Copie de l'adresse de l'accès pour affichage
                string acc_addr = _ << access->address();

                // Creation du node UNSAFE
                flux << "Unsafe_" << _count << " [label=\"{UNSAFE ";
                displayString(acc_addr, &flux);
                flux <<"|Lead to a hit @ ";
                displayString(addr, &flux);
                flux <<"}\", color = red];" << io::endl;

                // On met au même niveau le node Unsafe et le BB auquel il correspond
                flux << "{rank = same; Unsafe_" << _count << "; ";
                displayString(bb_match->cfg()->name(), &flux);
                flux << "_" << bb_match->cfg()->index() << "_" << bb_match->index() <<";}" << io::endl;   //TODO: à optimisé (1 par BB)
            }

        } else { // UNSAFE au début
            flux << "Unsafe_" << _count << " [label=\"{UNSAFE at the ENTRY|Can lead to a hit @ ";
            displayString(addr, &flux);
            flux << "|In function ";
            displayString(cfg->name(), &flux);
            flux << "}\", color = red];" << io::endl;
        }

        // On incrémente le compteur qui permet de nommer les nodes Unsafe
        _count++;
    } else {
        cerr << "ERROR: Unable to open the file" << io::endl;
    }
}

void DotModificator::finish(void)
{
    std::ofstream flux(_filename);

    if (flux){
        flux << "}" << io::endl;
    } else {
        cerr << "ERROR: Unable to open the file" << io::endl;
    }
}

}
