#ifndef LRUSECURITY_DOT_MODIFICATOR_H
#define LRUSECURITY_DOT_MODIFICATOR_H

#include <otawa/icache/features.h>
#include <otawa/base.h>
#include <otawa/cfg/CFG.h>
#include "AccMatching.h"

namespace lrusecurity
{

class DotModificator
{
    /* Cette classe permet de modifier un fichier DOT pour ajouter les résultats de l'analyse de sécurité*/
public:
    DotModificator(const char *dot_file, AccMatcher *match):_filename(dot_file), _accM(match), _count(0){};
    void modify(otawa::CFG *cfg, const otawa::icache::Access *access, otawa::Address address);
    void finish(void);
    inline AccMatcher *getMatcher(void){return _accM;};
    inline bool isFirst(void){return _count == 0;};

private:
    const char *_filename;
    AccMatcher *_accM;
    int _count;
};

}


#endif /* ifndef LRUSECURITY_DOT_MODIFICATOR_H */
