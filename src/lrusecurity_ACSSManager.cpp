#include <lrusecurity/ACSSecurityManager.h>

#include <otawa/icat3/features.h>
#include <lrusecurity/features.h>

#include "CacheState.h"

using namespace otawa;

namespace lrusecurity
{

ACSSecurityManager::ACSSecurityManager(WorkSpace* ws) :
	_underlyingManager(ws),
	_coll(**icat3::LBLOCKS(ws)),
	_b(nullptr)
{
	_states = new CacheState*[_coll.sets()];
	array::set(_states, _coll.sets(), null<CacheState>());
	_hasPrev = ws->isProvided(lrusecurity::EXIST_HIT_PREV_ANALYSIS_FEATURE);
}

ACSSecurityManager::~ACSSecurityManager()
{
	for(int i = 0; i < _coll.sets(); ++i)
		if(_states[i] != nullptr)
			delete _states[i];

	delete[] _states;
}

void ACSSecurityManager::start(otawa::Block* b)
{
	ASSERT(b);
	_b = b;
	_underlyingManager.start(b);
	_used.clear();
}

void ACSSecurityManager::update(const icache::Access& acc)
{
	icat3::LBlock* lb = icat3::LBLOCK(acc);
	CacheState& state = use(lb->set());
	if(_hasPrev)
		state._existHitPrevDomain.update(acc, *state._existHitPrevState, _underlyingManager);
}

int ACSSecurityManager::existHitAge(const icat3::LBlock* lb)
{
  return _underlyingManager.existHitAge(lb);
}

Address ACSSecurityManager::prev(const icat3::LBlock *lb)
  {
    if (! _hasPrev)
      return 0; //FIXME
    else
      return use(lb->set())._existHitPrevState->get(lb->index());
  }
void ACSSecurityManager::print(const icat3::LBlock* lb, Output& out)
{
	CacheState& state = use(lb->set());
	if(_hasPrev) {
		out << "{Prev: ";
		state._existHitDomain.print(*state._existHitState, out);
	}
}

CacheState& ACSSecurityManager::use(int set)
{
	if(!_used.contains(set)) {
		if(_states[set] == nullptr)
			_states[set] = new CacheState(_coll, set);
		if(_hasExistHitPrev) {
			if(_states[set]->_existHitState == nullptr)
				_states[set]->_existHitPrevState = new ExistHitPrevDomain::t;
			_states[set]->_existHitPrevDomain.copy(*_states[set]->_existHitState, (*EXIST_HIT_PREV_IN(_b))[set]);
		}
		_used.add(set);
	}

	return *_states[set];
}

} // namespace lrusecurity
