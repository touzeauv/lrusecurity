#ifndef LRUSECURITY_EXIST_HIT_PREV_ADAPTER_O_H_
#define LRUSECURITY_EXIST_HIT_PREV_ADAPTER_O_H_

#include <otawa/ai/ArrayStore.h>
#include <otawa/icat3/features.h>
#include <otawa/cfg/CompositeCFG.h>
#include <otawa/cfg/features.h>

#include <lrupreanalysis/features.h>

#include "ExistHitPrevODomain.h"

namespace lrusecurity
{

class ExistHitPrevOAdapter
{
public:
  using domain_t = ExistHitPrevODomain;
	using t = typename domain_t::t;
	using graph_t = otawa::CompositeCFG;
	using store_t = otawa::ai::ArrayStore<domain_t, graph_t>;

	ExistHitPrevOAdapter(int set, const t* init, const otawa::icat3::LBlockCollection& coll, const otawa::CFGCollection& cfgs, otawa::WorkSpace* ws);

	inline domain_t& domain(void) { return _domain; }
	inline graph_t& graph(void) { return _graph; }
	inline store_t& store(void) { return _store; }

	void update(const otawa::Bag<otawa::icache::Access>& accs, t& d);
	void update(otawa::Block *v, t& d);

private:
	lrupreanalysis::eh_em::ACSManager _mayManager;
	domain_t _domain;
	graph_t _graph;
	store_t _store;
};

} // namespace lrusecurity

#endif // LRUSECURITY_EXIST_HIT_ADAPTER_O_H_
