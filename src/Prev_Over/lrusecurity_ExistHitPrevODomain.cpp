#include "ExistHitPrevODomain.h"
#include <set>

using namespace otawa;

namespace lrusecurity
{

ExistHitPrevODomain::ExistHitPrevODomain(
        const icat3::LBlockCollection& coll,
        int set,
        const t *init):
    _n(coll[set].count()),
    _bot(_n),
	_top(_n, nullptr), //TO CHANGE
    _set(set),
	_coll(coll),
    _A(coll.A()),
    _init(init ? *init : _top),
    _tmp(_n)
{
}

bool ExistHitPrevODomain::equals(const t& acss, const t& bcss) const
{
  for (int i = 0; i < _n; i++)
    if (acss[i] != bcss[i])
      return false;
  return true;
}

void ExistHitPrevODomain::join(t& dcss, const t& scss)
{
  for (int i = 0; i < _n; i++) {
    dcss[i].insert(scss[i].begin(), scss[i].end());
  }
}

void ExistHitPrevODomain::fetch(t& acss, const icache::Access& access, lrupreanalysis::eh_em::ACSManager& mayManager)
{
  icat3::LBlock *lb = icat3::LBLOCK(access);
  int b = lb->index();
  for (int i = 0; i < _n; i++){
    if (i == b){
      acss[i] = std::set<const otawa::icache::Access *>();
      acss[i].insert(&access);
    } else if (mayManager.mayAge(_coll[_set][i]) == _A)
      acss[i] = std::set<const otawa::icache::Access *>();
  }
}

void ExistHitPrevODomain::update(const icache::Access& access, t& a, lrupreanalysis::eh_em::ACSManager& mayManager)
{
  switch(access.kind()) {

  case icache::FETCH:
    if(_coll.cache()->set(access.address()) == _set)
      fetch(a, access, mayManager);
      break;

  case icache::PREFETCH:
    if(_coll.cache()->set(access.address()) == _set) {
      copy(_tmp, a);
      fetch(a, access, mayManager);
      join(a, _tmp);
    }
    break;

  case icache::NONE:
    break;

  default:
    ASSERT(false);
  }
}

} // namespace lrusecurity
