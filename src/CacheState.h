#ifndef LRUSECURITY_CACHE_STATE_H_
#define LRUSECURITY_CACHE_STATE_H_

#include "Prev/ExistHitPrevDomain.h"

namespace lrusecurity
{

class CacheState
{
public:
	CacheState(const otawa::icat3::LBlockCollection& coll, int set) :
			_existHitPrevDomain(coll, set, nullptr),
			_PrevState(nullptr),
	{
	}

	~CacheState()
	{
		if(_existHitState)
			delete _PrevState;
	}

	ExistHitPrevDomain _existHitPrevDomain;

	typename ExistHitPrevDomain::t* _PrevState;
};

} // namespace lrusecurity

#endif // LRUSECURITY_CACHE_STATE_H_
