
CMAKE_MINIMUM_REQUIRED(VERSION 2.6)

# configuration
set(PLUGIN	"lrusecurity")	# plugin name
set(NAMESPACE	"")		# namespace
set(SOURCES	"src/lrusecurity.cpp"
  "src/lrusecurity_SecurityCatBuilder.cpp"
  "src/Prev/lrusecurity_ExistHitPrevAnalysis.cpp"
  "src/Prev/lrusecurity_ExistHitPrevDomain.cpp"
  "src/Prev/lrusecurity_ExistHitPrevAdapter.cpp"
  "src/Prev_Over/lrusecurity_ExistHitPrevOAnalysis.cpp"
  "src/Prev_Over/lrusecurity_ExistHitPrevOAdapter.cpp"
  "src/Prev_Over/lrusecurity_ExistHitPrevODomain.cpp"
  "src/Prev_Under/lrusecurity_ExistHitPrevUAnalysis.cpp"
  "src/Prev_Under/lrusecurity_ExistHitPrevUDomain.cpp"
  "src/Prev_Under/lrusecurity_ExistHitPrevUAdapter.cpp"
  "src/lrusecurity_Displayer.cpp"
  "src/Graphic/AccMatching.cpp"                        # Modification for Dot
  "src/Graphic/DotModificator.cpp"
)

# script
project(${PLUGIN})

# look for OTAWA
if(NOT OTAWA_CONFIG)
    find_program(OTAWA_CONFIG otawa-config DOC "path to otawa-config")
    if(NOT OTAWA_CONFIG)
        message(FATAL_ERROR "ERROR: otawa-config is required !")
    endif()
endif()

file(COPY ${CMAKE_CURRENT_SOURCE_DIR}/${PLUGIN}.eld DESTINATION ${CMAKE_CURRENT_BINARY_DIR}/)

message(STATUS "otawa-config found at ${OTAWA_CONFIG}")
execute_process(COMMAND "${OTAWA_CONFIG}" --cflags
		OUTPUT_VARIABLE OTAWA_CFLAGS
		WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
		OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND "${OTAWA_CONFIG}" --libs --make-plug ${PLUGIN} -r
		OUTPUT_VARIABLE OTAWA_LDFLAGS
		WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
		OUTPUT_STRIP_TRAILING_WHITESPACE)
execute_process(COMMAND "${OTAWA_CONFIG}" --prefix
		OUTPUT_VARIABLE OTAWA_PREFIX
		WORKING_DIRECTORY "${CMAKE_CURRENT_BINARY_DIR}"
		OUTPUT_STRIP_TRAILING_WHITESPACE)

# plugin definition
set(ORIGIN $ORIGIN)
add_library(${PLUGIN} SHARED ${SOURCES})
set_property(TARGET ${PLUGIN} PROPERTY PREFIX "")
set_property(TARGET ${PLUGIN} PROPERTY COMPILE_FLAGS "${OTAWA_CFLAGS} --std=c++11 -g -Wall -Wextra")
target_include_directories(${PLUGIN} PUBLIC "${CMAKE_SOURCE_DIR}/include")
target_link_libraries(${PLUGIN} ${OTAWA_LDFLAGS})

# installation
set(PLUGIN_PATH "${OTAWA_PREFIX}/lib/otawa/${NAMESPACE}")
install(TARGETS ${PLUGIN} LIBRARY DESTINATION ${PLUGIN_PATH})
install(DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/include/lrusecurity" DESTINATION "${OTAWA_PREFIX}/include/")
install(FILES ${PLUGIN}.eld DESTINATION ${PLUGIN_PATH})
